/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ex1;

/**
 *
 * @author dagaga1314daw2
 */
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Ex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
    }

}
